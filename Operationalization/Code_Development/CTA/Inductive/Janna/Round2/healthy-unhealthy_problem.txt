Példák Unhealthy - Healthy dilemmára: 

"Igen, hogy kinek mennyi pénze van, hogy milyen, milyen, milyen ételeket tud magának megvenni, igen, hát, öhm, de az is lehet, hogy nekik van egy családi gazdaságuk és ott tényleg mindent megtermelnek és nem dolgoznak, hanem, hanem mármint, hogy itt dolgoznak a családi gazdaságukban és akkor nem antibiotikummal etetik az állataikat és akkor meg egészségesebben élnek, simán ez is előfordulhat, hát, igen, de összekötöm a pénzt az étkezésre is, hogy ez az egészséget befolyásolja."

[[uid=7k29pbjr]] Itt is a kontrollált fogyasztás, tehát hogy nem kontroll nélkül tömjük a fejünket mindennel, amit csak elénk kerül. Húú erről, erről nagyon- nagyon hosszan tudnék beszélni, mert én gasztroenterológusnak készülök majd... [[META>ability]] [[NUTRI>unhealthy]] [[NUTRI>healthy]]

[[uid=7k29pbkc]] P: Jaa, hogy szeretnéd, hogy elmondjam a szabályt nagyon szívesen, tehát minél több zöldség és gyümölcs főleg őő nem mély, deepfride, ajj nem tudok magyarul, tehát hogy minél több zöldéget és gyümölcsöt tartalmazó ételeket együnk, a rost fogyasztás kifejezetten fontos a megfelelő bélműködés számára, emellé legalább napi 2-2, 5 liter tiszta vizet fogyasszunk. [[NUTRI>unhealthy]] [[NUTRI>healthy]]

[[uid=7k29pc73]] P Hát, ennyiből elég nehéz megmondani, lehetséges, hogy csak most szívnak el egy-egy szál cigit, de azért nem valószínű  és így a fast food meg az alkohol, dohánytermékek együtt, azért ezek nem jó párosítások, bár egyikőjük sincsen elhízva, akiket látok a képen, hát, nem is tudom, mit mondjak erről? [[NUTRI>unhealthy]] [[NUTRI>healthyWeight]]

Hát, hogyha felismeri ezeket a, ezeket a veszélyfaktorokat és tudatosan próbálja minimalizálni az életében, akkor ez azért egy igen nagy lépés lehetne, mert hát nem tudunk mindenről lemondani, amit szeretünk és nem is kell feltétlenül, de, de azért zöldségek fogyasztása, gyümölcsök fogyasztása, vitamin készítményekkel való kiegészítése, húsfogyasztás minimalizálása, változatos étrend, rostban gazdag táplálékok, ilyesmikre gondolok. [[NUTRI>unhealthy]] [[NUTRI>healthy]]

