Healthy/Unhealthy dichotomy ambiguity (?) examples:

### Example 1:
[[uid=7k29p9cs]] P:Úgyhogy, nyilván itt látszanak ezek a káros szenvedélyek már fiatal korban, dohányzás, alkoholfogyasztás, itt egy pizzás doboz, ami, hát alapvetően nem, nem tartom problémának, hogyha nyilván az ember egyszer-egyszer ilyen élelmeket fogyaszt, nyilván, én is szoktam ilyeneket enni, de, nyilván ez se egy, egy egészséges étel, de, de szerintem ez, egy elfogyasztott pizza, az nem nagyon határozza meg az embernek az egészségét.
[[uid=7k29p9ct]] És hát, mi van még itt?
[[uid=7k29p9cw]] Hát az úttesten ülnek.
[[uid=7k29p9cx]] Vagy nem tudom, szerintem ez egy úttest, de nem biztos.
[[uid=7k29p9cy]] Ha az úttesten ülnek, akkor az nem olyan jó dolog, de hogyha nem, akkor semmi gond nincsen.
[[uid=7k29p9cz]] Meg, meg ami így nekem az eszembe jut, az, hogy így, ugye a társaság, ugye az embereket körülveszik a barátaik.
[[uid=7k29p9d0]] És hogy ez, ez egyébként szerintem egy nagyon, nagyon pozitív dolog, hogyha az ember egy ilyen jó társaságban tud felnőni, habár ugye itt, itt lehet szó akár ilyen társasági nyomásról is, akár a dohányzás, akár az alkoholfogyasztás tekintetében, szóval, hogy nem mindegy, hogy a baráti társaságnak milyen szokásai vannak.

### Example 2:
kifejtenéd kérlek, hogy a biztonságos szex alatt mit értesz?
P: Hát azt, hogy, hogy a nem kívánt terhességet meg tudjuk előzni.
Egyrészt, ugye, és ugye ne kelljen elmenni abortuszra évente, vagy fél évente, vagy ilyesmi.

### Example 3:
[[uid=7k29p9z2]] P: Hát ezt a pizzás dobozt említettem, ami egy részről jó, mert hogyha iszunk akkor jó hogyha eszünk is mellette és nem csak halálra isszuk magunkat és valami felszívja az alkoholt, őő mondjuk a pizza nem feltétlen a legrosszabb választás ebből a szempontból, ühm hátha ilyen szénhidrát csökkentett tésztából van, ami nem is olyan rossz, meg ha tele van zöldséggel.

### Example 4:
Igen, hogy kinek mennyi pénze van, hogy milyen, milyen, milyen ételeket tud magának megvenni, igen, hát, öhm, de az is lehet, hogy nekik van egy családi gazdaságuk és ott tényleg mindent megtermelnek és nem dolgoznak, hanem, hanem mármint, hogy itt dolgoznak a családi gazdaságukban és akkor nem antibiotikummal etetik az állataikat és akkor meg egészségesebben élnek, simán ez is előfordulhat, hát, igen, de összekötöm a pénzt az étkezésre is, hogy ez az egészséget befolyásolja.

### Example 5:
Igen, hát, itt beszélhetünk egyik oldalról a szexuális úton terjedő betegségekről, amiről ugye nagyon sokan egyébként nem tudnak, nem úgy tudnak, nem veszik komolyan, nem tudják, hogy, hogy kell ez ellen védekezni, nem értik a lényegét, nem kezelődnek ki, ha már kiderült valami ilyesmi, szóval, hogy ez szerintem mind-mind ide tartozik, mint szexuális úton terjedő betegségek illetve a hosszú távú következményeiről nem tudnak az emberek sokszor, ez is fontos, aztán hát, a fogamzásgátlás, ami így szexuális témakörben egy nagyon fontos dolog

### Example 6:
Alapvetően szerintem annyira így benne van a társadalomban ez az alkohol, ami nem feltétlenül egy jó dolog, de, hogy ez valami nagyon, nagyon elfogadottá vált, az, hogy az emberek alkoholt fogyasztanak, meg, meg tényleg egy kismértékű alkoholfogyasztás az, az nem jelent jelentős egészségügyi kockázatot szerintem, és, szerintem itt a képen nem, nem azt a fajta alkoholfogyasztást látjuk, ami így az egészségre káros lenne.

### Example 7:
[[uid=7k29pbjr]] Itt is a kontrollált fogyasztás, tehát hogy nem kontroll nélkül tömjük a fejünket mindennel, amit csak elénk kerül. Húú erről, erről nagyon- nagyon hosszan tudnék beszélni, mert én gasztroenterológusnak készülök majd...

### Example 8:
hanem például biciklivel járják a falut vagy akár a faluk közötti távolságokat is, ami egészség szempontjából tökre hasznos, vagyis én emlékszem rá, hogy, hogy nekem is például a nagyváros, Nyíregyháza, azért az elég messze volt busszal, viszont falun belül vagy falvak között biciklivel ezek mindig könnyen elérhetőek voltak egyébként a barátok is szóval én is sokat bicikliztem, amikor még otthon laktam, ellenben itt Budapesten soha nem merek ráülni egy biciklire, mert rögtön azt látom, hogy mikor fognak elütni és ott fogok meghalni.

### Example 9:
[[uid=7k29pbkc]] P: Jaa, hogy szeretnéd, hogy elmondjam a szabályt nagyon szívesen, tehát minél több zöldség és gyümölcs főleg őő nem mély, deepfride, ajj nem tudok magyarul, tehát hogy minél több zöldéget és gyümölcsöt tartalmazó ételeket együnk, a rost fogyasztás kifejezetten fontos a megfelelő bélműködés számára, emellé legalább napi 2-2, 5 liter tiszta vizet fogyasszunk.

### Example 10:
[[uid=7k29pbcm]] Viszont, van egy bicikli a képen, amibe nagyon pozitív, mert itt azért még sokan járnak biciklivel erre arra, ugye nyilván a távolságok sem ugyanazok de, hogy ez jobban a mindennapok része és egyébként ez egy egészséges dolog, öhm, a fizikai munka, amit egy ilyen ház körül lehet, így egy ilyen tanyasias, falusias világban, muszáj az embernek erre szocializálni, szocializálódnia, öhm szerintem ez ez nagyon fontos, öhm nagyon fontos, mert szerintem az ember érzi a két kezével elvégzett munkának az eredményét és egyébként szerintem ez, ezt fontos érezni, fontos megtapasztalni, ad ad fizikumot is valamilyen szempontból, meg állóképességet, öhm szerintem sokat számít a hozzáállásban, tehát az élethez való hozzáállásban, talán kicsit kialakít egy ilyen, itt van ez a munka, el kell végezni hozzáállást, öhm aminek lehet, hogy vannak hátrányai is egyébként, ez most így hirtelen nem tudnék mondani, úgyhogy szerintem ez fontos, nyilván vannak ennek is hátrányai mert, hogyha az ember reggeltől estig robotol és leszakad a dereka és nem tudtad, hogy az egészséges életmódnak azért nem ez a teljes kulcsa, nem a fizikai munka, de de igenis szerintem sokat tud számítani adni az embernek, ami rizikófaktor az mondjuk, hogy nem ezek az emberek, nem nagyon szoktak a nap ellen védekezni, úgyhogy öhm itt a nyári munkálatok során sok ilyen leégés és hasonló történik, ami szintén ugyanúgy a természetes, öhm dolognak vesznek és igazából aztán egy idő után ebből alakulhat ki valami olyan, amit aztán, amit aztán kórházba kell kezelni, de öhm azt hiszem, hogyöh, ami még tényező, az szerintem viszont a friss levegő, meg a természetközeli környezet, szerintem ez az, hogy ezt valaki értékelni tudja, öhm az szerintem nagyon nagyon fontos, mert mert vissza tud térni bele, vissza tud kapaszkodni, lehet, hogy elkerül egy nagyvárosba és lehet, hogy öhm végül egyébként ott így megtalálja az örömét, de de tudja, hogyha sok és elég volt, és nagyon stresszes, akkor tudja, tudja, hogy hova menjen tudja, ami biztonságot ad, ami megnyugtatja, ami otthonos érzéssel tölti el és és felszabadítja és akkor csak visszamegy, visszamegy a zöldbe és tudja értékelni és ott van ez mélyen elültetve benne, szerintem ez, ez óriási dolog.

### Example 11:
[[uid=7k29pc73]] P Hát, ennyiből elég nehéz megmondani, lehetséges, hogy csak most szívnak el egy-egy szál cigit, de azért nem valószínű  és így a fast food meg az alkohol, dohánytermékek együtt, azért ezek nem jó párosítások, bár egyikőjük sincsen elhízva, akiket látok a képen, hát, nem is tudom, mit mondjak erről?

### Example 12:
Hát, hogyha felismeri ezeket a, ezeket a veszélyfaktorokat és tudatosan próbálja minimalizálni az életében, akkor ez azért egy igen nagy lépés lehetne, mert hát nem tudunk mindenről lemondani, amit szeretünk és nem is kell feltétlenül, de, de azért zöldségek fogyasztása, gyümölcsök fogyasztása, vitamin készítményekkel való kiegészítése, húsfogyasztás minimalizálása, változatos étrend, rostban gazdag táplálékok, ilyesmikre gondolok.



